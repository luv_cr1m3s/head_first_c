## Notes for the first part of `HEAD first C`.

### Chapter 1
---

* There is difference in `%i` and `%d` for only `scanf`:
> `%d` takes integer value as signed decimal integer i.e. it takes negative values along   
> with positive values but values should be in decimal otherwise it will print garbage 
> value. Consider a following example.  

> `%i` takes integer value as integer value with decimal, hexadecimal or octal type.  
> To enter a value in hexadecimal format – value should be provided by preceding `0x` and   
> value in octal format – value should be provided by preceding `0`.  

* `\0` called as `NULL` character.
* String literals are constant unlike character arrays. In case if you gonna try to change string `gcc` will report `bus error`.
* The `C99` standard allows to use `true` and `false` words, but complier treats them as `0` and `1`.

* It's impossible to use `||` or `&&` in switch statement, but you can use instead such construction:
`
switch(value){
    case a:
    case b:
    case c:
        value = val;
        break;
        ...
}`

* `switch` could be used only for single values and not for the arrays/strings.
* `break` don't breaks out of `if`, it breaks out of code block.
* Chain assignment `x = y = 4`.

### Chapter 2
---

* Variables declared in functions stored in `stack`, variable declared out of function stored in `global`.
* To find memory address of the variable use `&` before the variable, `%p` used to format address in `hex` format.
`int x = 1;
printf("Variable x stored at %p.\n", &x);`

* When you call a function, you don't send the `variable` as argument, just its `value`.

#### Pointers
---

* A pointer variable is just variable that stores a memory address:
`int *address = &address;`

* To read data stored in `pointer variable` you need to use `*`:
`int value_stored = *address;`

* `*` and `&` operators are opposites. The `&` takes data and shows where the data
stored, and `*` takes an address and tells whats stored.

* To manipulate stored data use `*`:
`*address = 99;`

#### Arrays
---

* When you are passing a string to function in fact you are passing just pointer.
`char quote[] = "Hello world!";
printf("The quote string stored at:%p\n", quote);` -- quote without braces become a pointer.

* Both `array[0]` and `*array` refers to the first element of the array i.e.:
` drinks[0] == *drinks;`

* Because of number is just a number, you can make arithmetic operations on it:
`drinks[2] == *(drinks + 2);`

* To print array stating with some special character you can use:
`puts(*(array + 2));`

* So `array[3]` could be written as:
`array[3] == *(array + 3) == *(3 + doses) == 3[doses]`

* Pointer variable have different types for each type of data.
* Passing an array variable to a pointer decays it.

* `scanf` can understand input and can take structured data:
`scanf("%19s %19s", first, second);`

* You can get `segmentation fault` or `abort trap` if use just `%s` instead of `%<n>s` in `scanf`.

* `fgets` could be used as alternative for `scanf`, it forces you to put length of string:
`fgets(some_string, sizeof(some_string), stdin);`

* A variable that points to a string literal can't be used to change the contents of the string:
`char *cards = "JQK";` -- can't be modified
* But array made from a string literal can be modified:
`cahr cards[] = "JQK";`

* There is no difference between form of declaration:
`void some_f(char cards[]) == void some_f(char *cards)` -- in both cases string is immutable.

* Good practice to use `const` when declaring string literal to get compile error:
`const char *s = "some";` -- will raise `assignment of read only memory` when something 
will try to modify it.

* Stack used to store variables and it's grows `downward`.
* Heap used for dynamic memory.
* Globals special `piece` of memory where stored global variables that visible for all functions.
* Constants are stored in read-only memory (like `string literals`).
* Code lying down under all other data, place where actual assembled code gets loaded.

### Chapter 2.5 `<string.h>`
---

* Be careful with `fgets` it reads `\n` from input and adds it to string, so check it:

```c
char search[<n>];
fgets(search, <n>, stdin);
char *pointer;
if ((pointer = strchr(search, '\n')) != NULL){
    *pointer = 0;
}
```

* Check `reverse.c` to see cool realization of `printf_reverse`.
* Some useful function from `<string.h>`: 
> `strcmp` -- compare two strings,  
> `strcat` --concatenate two strings,  
> `strcpy` -- copy one string to another,  
> `strstr(a, b)` -- return address of string a in b, or `NULL`,  
> `strlen` -- returns length of the string,  
> `strchr` -- finds the location of char in the string.  

### Chapter 3.
---

* The easiest way to input/output data for c program in terminal is to use `<` and `>` operators.
* `printf` sends data to `stdout` it's just default version of `fprintf`.

* Interesting example of `input/output` redirection:

```bash
(./app1 | ./app2) < input.txt > output.txt
```

* Functions for work and redirection of input/output into files:
> `FILE *file = fopen("<name>", "r/w/a")` -- always a pointer.  
> `fscanf(*file, "<string formati>", <variable to store>)`  
> `fprintf(*file, "<string format>", value1, value2...)`  
> `fclose(*file)` -- be sure to close the file, max amount of files that you can open ~256

* `unistd.h` not a part of standard library, but it gives access to some of `POSIX` libraries.
* `getopt()` function lets iterate through the arguments given to the program:
> `getopt(argc, argv, "ae:"` -- '`ae:`' is example of options, `:` means that option needs argument  
> `getopt` stores options argument in `optarg` variable.  
> after reading of all options skip them using `optind` variable.  

* After processing the arguments, `argv[0]` won't stand for the program name anymore.
* `getopt` can handle combined arguments like `-ltd`.
* To use negative numbers as option in command line separate them with `--` like:
`<progname> -ab -- -1`

### Chapter 4
---

* `printf` format for different data types:
> `lf` for double  
> `zu` to print value in bytes  
> `hi` to print short value 

* Declaration of functions without definition especially useful in case of recursive functions.
* Including functions as header file could be even better.

* Compiler assume that non declared function return `int`.

* To include variable from another file use `extern` keyword for variable declaration.

* In case if you use parts of program in `*.h` it would be useful to create `*.o` files 
to escape recompiling of all files in case when you make some changes into one of them.
> `ggc -c *.c` -- creates not linked object files.  
> `gcc *.o -o <progname>` -- links together all object files.  

* Linking is much more faster than a compiling.

* All of the recipes lines in makefile must start with a tab.
* `make` can track time creation, so it will recompile only files that actually need it.
* By default `make` is looking for filename `[M|m]akefile` to use file with another name use option `-f`.  

### Chapter 5
---

* Structure creation:

```c
struct <struct name>{
    params...
    };
```
* Structure call:

```c
struct <struct name> <unit name> = { param1, param2 ...};
```
* You can access structure parameters only by name:
`<unit name>.<parameter name>`

* When you're assigning `struct` variables, you are telling to copy data.

* `typedef` creates alias for structure:

```c
typedef struct cell_phone{
    int cell_no;
    int char *wallpaper;
} phone;
```
> After that you can omit using `struct cell_phone` for creating variable  
> and just type `phone p = ...`.  
> also you can omit using `cell_phone` by just skipping it.  

* In `C` parameters are passed to functions `by value`. So when you passing `struct` into function it's getting copied.
* If you want to change some parameter of `struct` you need to send pointer to that `struct`.
> You need send `struct variable` as `&<variable>` to function.  
> You can get access to parameters by using `(*variable).parameter` format.  
> Also you can use `->`: `var->par` == `(*var).par`.  

* `union` used to store different types of data:

```c
typedef union{
    int val1;
    float val2;
    char var3;
    ...
} union_name;
```

* To get access to the elements of union you can use:

> `union_name a = {4};` -- will set var1 to 4.  
> `union_name a = {.var2 = 1.4};`  
> `union_name a`  
> `a.var3 = 'f'`  

* You cannot declare `union` in such way:

```c
union_name a;
a = {2, 1.2, 'f'};
```

* Compiler will see it as array, not as union.

* `enum` creates set of values:
> `enum colors { RED, BLUE};` -- used comas.  
> C gives each of the symbols a number starting at 0, `RED == 0` and `BLUE == 1`.      
> `enum colors favorite = RED;` -- you can assign only values from the set.  

* `C` doesn't support binary literals, only hexadecimal.

* `bitfields` used to store a custom number of bits, must be declared as `unsigned int`:

```c
typedef struct{
    unsigned int first:1;
    unsigned int second:4;
    unsigned int third:3;
} synth;
```

* Compiler can squash bitfields together -> save space.
* `bitfields` could be used to read low-level binary data.

### Chapter 6
---

* For a recursive structures you need to include pointer to the same type, so compiler 
won't let you use `typedef` alias -> structure should have proper name.

* `->` gives access to the parameter of structure.

* `malloc` reserves some space in heap memory and returns pointer to that reserved part of memory.
* `heap` has only a fixed amount of memory available.
* After use memory should be released wit `free` function or you will face memory leak.

* You cannot just sent a string pointer to the function and expect normal behavior. Instead you should use `strdup` function from `string.h`.
* `strdup` is not standard C function, so you need to compile your code with `-std=gnu11`.

* `valgring` used to find memory leaks.
* To add debug info into executable compile it with `-g` switch.
* To check executable for memory leaks run `valgrind --leak-check=full <exec_name>`.  

### Chapter 7
---

* Every function name is a pointer to the function. Example of creating pointer to the function:

```c
int go_to_w(int speed){
    return speed*100;
}

int (*warp_fn)(int); // 1. return type 2.pointer 3. function param type
warp_fn = go_to_w; //  warp_fn stores the address of the go_to_w function
warp_fn(4); // calling go_to_w
```
* `shoot()` and the `&shoot` are both pointers to the function `shoot`.  
* You can use those function pointers as `f_p(<param>)` or `(*f_p)(<param>)`.

* `char**` points to an array of strings.
* `void*` pointer can point to anything.
* `qsort` -- function from standard library that can be used to sort elements in array. 
* `qsort` modifies the original array!!!!!

* Creation of array of pointers to functions:
`void (*replies[])(response) = {dump, second, third}` 
> 1. `void` -- return type.  
> 2. `(*replies[])` -- variable name.  
> 3. `(response)` -- parameter taken by the functions.  
> 4. `{dump, second...};` -- names of the functions.  

* Usage of elements from such array:
`replies[i](response)`  
* `variadic function` -- function that takes variable number of arguments. To declare it use `stdarg.h`.  
* `macro` used to rewrite code before compilation. `va_list`, `va_start`, `va_arg`, `va_end` used for `variadic function` declaration.  
* You need at least one fixed parameter. Be careful with reading parameters.

### Chapter 8
---

* Standard libraries stored in `/usr/local/include` or `/usr/include`.  

> `nm` could be used to check content of archives.  
> `ar` could be used to create/extract archives.  

> `gcc -test.c -lsecurity -o somewhere`   
* `-lsecurity` stands for `libsecurity.a` stored in standard library directories.
> `ar -rcs libsecurity.a encrypt.o checksum.o` -- way of creating archives.  
* You can tell compiler where to look for archive by adding `-Ldir_name` flag.
* `-Idirname` will show where to look for headers files.  

* You should keep some order when using `gcc`, or you will get linker errors:
`gcc secure.c -o binary/secure -Lpath -Ipath -llibname`

* I'm not sure that I will remember it but `dynamic linking`:)

* Dynamic libraries are linked to programs at runtime. 
* To create dynamic linked object file create an object file:
> gcc -I/includes `-fPIC` -c hfcal.c -o hfcal.o
> `-fPIC` stands for `position-independent code` and not really needed.  

* On Linux `dynamic library` called `shared object files` and have extension `.so`:
`gcc -shared hfcal.o -o libs/libhfcal.so`  
* You will need manually setup path to the directory with the `.so` lib:
`export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:libs`

### Chapter 9
---

* `system` function could be used to execute command line commands.
* Usage of `system` could cause a lot problems starting with security, undefined behaviour...

* No processes on the system without kernel loading it into memory.
* Kernel uses device drivers to talk to the equipment.
* System calls are the functions that program uses to talk to the kernel.

* `exec` function replace the current process. You can chose by `pid` which process should be replaced.
* `exec` belongs to `unistd.h`. All versions of `exec` function belongs to the two groups: `list` and `array` functions.
* The list functions: `execl`, `execlp`, `execle` -- all of them accept command-line arguments as a list of functions.
> 1.`execl(`2.`"/home/clu", `3.`"/home/clu", "paranoids", "contract", `4.`NULL)`  
> 1. execute a list of arguments.  
> 2. name of the program.  
> 3. same string as a first parameter.  
> 4. the last parameter always NULL.  

* The array functions: `execv`, `execvp`, `execve`.
> `execvp("clu", my_args);`  
> 1.  execute vector of arguments and search on the PATH.  
> 2. first argument name of the program.  
> 3. Second argument array of arguments.  

* You can get access to the system variables with `getenv` function. 
* `errno` -- system variable that keeps error value.
* `strerror` function from `string.h` that converts errors into text:
`puts(strerror(errno))`
* System calls as usual in fail case return `-1` and set `errno` with some value.

* You can make duplicate of the process by calling `fork` function.
`pid_t pid = fork();`

### Chapter 10
---

* File descriptors don't necessarily refers to files, it's just a data stream.

* Standard `IOE` are always fixed in the same places in the description table. But the data streams they point to can change.

* Processes can redirect themselves by rewriting the descriptor table.

* `fileno` function returns number of the file in descriptor table(one of the few system function that doesn't return -1 in fail case):
`int descriptor = fileno(filename);`

* `dup2` function can duplicate data stream from one slot to another.
> `dup2(4, 3)` -- duplicates data stream from file descriptor 4 to 3.  

* Calling `exit` function will stop program without worry about returning to `main`.

* To be sure that process called by function `exec` finished use `waitpid` function:
`waitpid(pid, pid_status, options);`

* You can get `pid_status` by using `WEXITSTATUS` macro:
`WEXITSTATUS(pid_status);`

* Redirecting input and output, and making wait for each other, are all simple forms of `interprocess communication`.

* Whenever you pipe commands together on the command line, you are actually connecting them as parent and child.

* `waitpid` waits for a process to finish.

* OS controls programs with signals. `Ctrl+C` sends interrupt signal that forces default interrupt handler to call `exit()`.

* You can handle signals sent by used or OS with `sigaction`.
* A `sigaction` is a `struct` that contains a pointer to a function.
* `sigactions` are used to tell the operating system which function it should call when a signal is sent to a process.

```c
struct sigaction action;
action.sa_handler = diediedie;
sigemptyset(&action.sa_mask);
action.sa_flags = 0;
```
* Signals are just integer values, and if you create a custom handler function, it will need to accept an int argument.
* Check `signald` that's OS can send to processes.
* You can test signal handling in your program by sending signals with `kill`.
`kill [-INT/-SEGV/-KILL...] pid` 
* Handlers can be replaced with the `signal` function.
* You can make process to send signal by using `raise` function (it's called `signal escalation`).
* The interval timer sends `SIGALARM` signals. The `alarm` function sets the interval timer.

### Chapter 11
---

* To write program for network you need to use `sockets` that could be found in `<sys/socket.h>`.
* Before server can use a socket to talk to a client program, it needs to go through four stages `BLAB: Bind to a port, Listen, Accept a connection, Begin talking`.
* For work with ports you need to use `<arpa/inet.h` library.

* You can find a list of used ports in `/etc/services`.
* When someone types text in telnet, the string always ends with `\r\n`.

* The `recv()` return the number of characters, or -1 if there's an error, or 0 if the client has closed the connection.

* You can let multiple users access server and use it by `fork()`ing process.
* To connect socket to a remote domain name you need a `getaddrinfo()` function from `netdb.h`.

### Chapter 12
---

* Library for creating `POSIX threads`: `pthread` (it's proprietary library, so you will need to use `-lpthread` flag for `gcc`.
* Thread function need to have a `void*` return type.

* Be afraid of thread unsafe programs, make your program safe with `mutexes`.
* You will need to lock values, work with them and unlock after use.
* You can write code that use multithread even on `singlecore` machine.
* As usual `threads` are faster than `processes`.
* Be afraid of deadlocks created by `mutex`.

* Take a look on:
> `gprof` -- tool for performance check.  
> `gcov` -- tool for execution process check.  

