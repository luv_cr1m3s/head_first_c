#include <stdio.h>
#include <string.h>
#include "island.h"

int main(){

    /* Islands creation */
    island amity = { "Amity", "09:00", "17:00", NULL };
    island craggy = { "Craggy", "09:00", "17:00", NULL };
    island isla_numblar = { "Islan Numblar", "09:00", "17:00", NULL };
    island shutter = { "Shutter", "09:00", "17:00", NULL } ;
    island skull = { "Skull", "09:00", "17:00", NULL };

    /* Islands linking*/
    amity.next = &craggy;
    craggy.next = &isla_numblar;
    isla_numblar.next = &skull;
    skull.next = &shutter;
   
    display(&amity);

    return 0;
}
