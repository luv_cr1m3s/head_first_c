#include <stdio.h>

enum response_type {DUMP, SECOND_CHANCE, MARRIAGE};

typedef struct{
    char *name;
    enum response_type type;
} response;

void dump(response r){

    printf("Dear %s,\n", r.name);
    printf("Unfortunately your late date contacted us to");
    printf("say that they will not be seeing you again\n");
}

void second_chance(response r){

    printf("Dear %s,\n", r.name);
    printf("Good news: your last date has asked us to");
    printf("arange another meeting. Please call ASAP.\n");
}

void marriage(response r){

    printf("Dear %s,\n", r.name);
    printf("Congratulations! Your last date has contated");
    printf("us with proposal of marriage.\n");
}

/* functions pointers array: */
void (*replies[])(response) = {dump, second_chance, marriage};

int main(){
    
    response r[] = {
        {"Mike", DUMP}, {"Luis", SECOND_CHANCE}, 
        {"Matt", SECOND_CHANCE}, {"William", MARRIAGE}
    };
    
    int array_len = sizeof(r) / sizeof(r[0]);

    for(int i = 0; i < array_len; i++){
        replies[r[i].type](r[i]);        
    }

/* Old version without using of function pointers array:
 *
    for(int i = 0; i < 4; i++){
        switch(r[i].type){
            case DUMP:
                dump(r[i]);
                break;
            case SECOND_CHANCE:
                second_chance(r[i]);
                break;
            default:
                marriage(r[i]);
        }
    }
*/
    return 0;
}
