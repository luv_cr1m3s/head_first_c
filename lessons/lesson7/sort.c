#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int width;
    int height;
} rectangle;

int compare_areas(const void* a, const void* b){
    rectangle* ra = (rectangle*)a;
    rectangle* rb = (rectangle*)b;

    int squarea = (ra->width * ra->height);
    int squareb = (rb->width * rb->height);
    return squarea - squareb;
}

int compare_areas_desc(const void* a, const void* b){
    return compare_areas(b, a);
}

int compare_names(const void* a, const void* b){
    char** str_a = (char**)a;
    char** str_b = (char**)b;
    return strcmp(*str_a, *str_b);
}

int compare_names_desc(const void* a, const void* b){
    return compare_names(b, a);
}

int compare_scores(const void* score_a, const void* score_b){
    
    int a = *(int*)score_a;
    int b = *(int*)score_b;
    return a - b;
}

int compare_scores_desc(const void* score_a, const void* score_b){
    int a = *(int*)score_a;
    int b = *(int*)score_b;

    return b - a;
}

int main(){
    printf("___---------------------------------____\n"); 
    int scores[] = {543, 323, 32, 554, 11, 3, 112};
    size_t length = sizeof(scores) / sizeof(int);
    qsort(scores, 7, sizeof(int), compare_scores);
    
    for(int i = 0; i < length; i++){
        printf("Element#%d: %d\n", i, scores[i]);
    }
    
    printf("___---------------------------------____\n");
    qsort(scores, 7, sizeof(int), compare_scores_desc);
    
    for(int i = 0; i < length; i++){
        printf("Element#%d: %d\n", i, scores[i]);
    }

    printf("___---------------------------------____\n");   
    char* names[] = {"Ghi", "Abc", "Def"};
    qsort(names, 3, sizeof(char*), compare_names);
    for(int i = 0; i < 3; i++){
        printf("%s\n", names[i]);
    }
    
    printf("___---------------------------------____\n");
    qsort(names, 3, sizeof(char*), compare_names_desc);
    for(int i = 0; i < 3; i++){
        printf("%s\n", names[i]);
    }
    
    printf("___---------------------------------____\n");
    rectangle a = {a.width=4, a.height=5};
    rectangle b = {b.width=6, b.height=3};
    rectangle c = {c.width=7, c.height=4};
    
    rectangle rec_arr[] = {a, b, c};

    qsort(rec_arr, 3, sizeof(rectangle), compare_areas);
    for(int i = 0; i < 3; i++){
        printf("Rectangle: %d\n", rec_arr[i].width*rec_arr[i].height);
    }

    printf("___---------------------------------____\n");
    qsort(rec_arr, 3, sizeof(rectangle), compare_areas_desc);
    for(int i = 0; i < 3; i++){
        printf("Rectangle: %d\n", rec_arr[i].width*rec_arr[i].height);
    }
    return 0;
}
