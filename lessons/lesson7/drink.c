#include <stdio.h>
#include <stdarg.h>

enum drink {
    MUDSLIDE, FUZZY_NAVEL, MONKEY_GLAND, ZOMBIE
};

double price(enum drink d){
    switch(d){
        case MUDSLIDE:
            return 6.79;
        case FUZZY_NAVEL:
            return 5.31;
        case MONKEY_GLAND:
            return 4.82;
        case ZOMBIE:
            return 5.89;
    }
    return -1;
}

double sum_price(int args, ...){
    va_list ap;
    va_start(ap, args);
    double result = 0;

    for (int i = 0; i < args; i++){
/* it takes int because elements in drinks has values 0,1... */
        result += price(va_arg(ap, int)); 
    }

    va_end(ap);
    return result;
}

int main(){
    
    printf("%.2f\n", price(MUDSLIDE));
    double bill = sum_price(3, MUDSLIDE, MONKEY_GLAND, FUZZY_NAVEL);
    printf("%.3f\n", bill);
    return 0;
}

