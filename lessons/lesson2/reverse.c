#include <stdio.h>
#include <string.h>

void printf_reverse(char *s){
    size_t len = strlen(s);

    char *t = s + len - 1;

    while(t >= s){
        printf("%c", *t);

        t = t - 1;
    }
    puts("");
}

int main(){

    char word[5] = "World";
    printf_reverse(word);

    return 0;
}
