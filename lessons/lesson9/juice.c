#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]){
    // setup environment variables, last element should be NUlL
    char *my_env[] = {"JUICE=peach and apple", NULL};

    printf("Dinners %s\n", argv[1]);
    // read system variable JUICE with getenv funtion
    printf("Juice %s\n", getenv("JUICE"));

    // execute program with created environment variables
    //execle("binary/juice", "binary/juice", "4", NULL, my_env);
    return 0;
}



