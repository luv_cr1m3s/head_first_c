#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

void error(char *msg){
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(1);
}

void* does_too(){
	int i = 5;
	while ( i >= 0){
		printf("Does too!\n");
		sleep(1);
		i--;
	}
	return NULL;
}

void* does_not(){
	int i = 5;
	while ( i >= 0){
		printf("Does not!\n");
		sleep(1);
		i--;
	}
	return NULL;
}


int main(){
  // Here will be stored information about threads
	pthread_t t0;
	pthread_t t1;
	
	// Creation of threads
	if(pthread_create(&t0, NULL, does_not, NULL) == -1)
		error("Can't create thread t0");
	if(pthread_create(&t1, NULL, does_too, NULL) == -1)
		error("Can't create thread t1");                 
	
	// Storage for void pointers returned from the functions
	void *result;
	if (pthread_join(t0, &result) == -1)
		error("Can't join thread t0");

	if (pthread_join(t1, &result) == -1)
		error("Can't join thread t1");
	return 0;
}
