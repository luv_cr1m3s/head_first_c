#include <stdio.h>

int main(){

    char info[80];
    float lat, lon;

    while(scanf("%f,%f,%79[^\n]", &lat, &lon, info) == 3)
        if( lat > 26 && lat < 34)
            if ( lon > -76 && lon < -64 )
                printf("%f,%f,%s\n", lat, lon, info);

    return 0;
}
