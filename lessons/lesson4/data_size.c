#include <stdio.h>
#include <limits.h>
#include <float.h>


int main(){
    
    printf("INT_MAX is %i\n", INT_MAX);
    printf("INT_MIN is %i\n", INT_MIN);
    printf("An int takes %zu bytes\n", sizeof(int));

    printf("FLT_MAX is %f\n", FLT_MAX);
    printf("FLT_MIN is %.50f\n", FLT_MIN);
    printf("An float takes %zu bytes\n", sizeof(float));

    printf("DBL_MAX is %lf\n", DBL_MAX);
    printf("DBL_MIN is %.50lf\n", DBL_MIN);
    printf("An float takes %zu bytes\n", sizeof(double));
    
    printf("CHAR_MAX is %i\n", CHAR_MAX);
    printf("CHAR_MIN is %i\n", CHAR_MIN);
    printf("An float takes %zu bytes\n", sizeof(char));
    return 0;
}
