#include <stdio.h>
#include "rand_caesar.h"
#include "encrypt.h"

int main(){
    char message[80];
    int code;
    FILE *ciphers = fopen("chipres.txt", "w");

    while(fgets(message, 80, stdin)){
        code = rand_caesar();
        encrypt(message, code);
        printf("Code is: %i. Message: %s\n", code, message);
        fprintf(ciphers, "%s\n", message);
    }
    
    fclose(ciphers);

    return 0;
}

