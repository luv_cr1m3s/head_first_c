#include <stdio.h>
#include "encrypt.h"

int main(){
    
    /* values used by encrypt 97-122 */

    FILE *in;
    if( !(in = fopen("chipres.txt", "r"))){
        fprintf(stderr, "Can't open the file.\n");
        return 1;
    }
    char *message;
    char *sub_message;

    while(fscanf(in, "%s", message) == 1){
        for(int i = 1; i < 26; i++){
            sub_message = message;
            encrypt(sub_message, i);
            printf("Code is: %i, message is: %s\n", i, sub_message);
        }
    }

    return 0;
}


