#include <stdio.h>

struct preferences{
    const char *food;
    float exercise_hours;
};

struct fish{
    const char *name;
    const char *species;
    int teeth;
    int age;
    struct preferences care;
};

void fish_info(struct fish f);
void fish_care(struct fish f);

int main(){
    
    struct preferences piranha = {"Meat", 7.5};
    struct fish snappy = {"Snappy", "Piranha", 69, 4, piranha};
    fish_info(snappy);
    printf("\n");
    fish_care(snappy);
    return 0;
}

void fish_info(struct fish f){

    printf("Info about %s fish:\n", f.name);
    printf("Species type: %s\n", f.species);
    printf("Age: %i, number of teeth: %i.\n", f.age, f.teeth);
}

void fish_care(struct fish f){
    printf("Instructions for %s care:\n", f.name);
    printf("%s prefer as food: %s\n", f.name, f.care.food);
    printf("%s shoould make exercises for %.1f hours.\n", f.name, f.care.exercise_hours);
}
