#include <stdio.h>

typedef struct {
    unsigned int first_visit: 1;
    unsigned int come_again:1;
    unsigned int fingers_lost:4;
    unsigned int days_a_week:3;
} survey;

int main(){

    survey a = {.first_visit=0x1,.come_again = 0x1, .fingers_lost=0xb, .days_a_week=0x5};
    printf("%d -- visit\n%d -- again\n%d -- fingers\n%d -- days\n",
            a.first_visit, a.come_again, a.fingers_lost, a.days_a_week);


    return 0;
}
