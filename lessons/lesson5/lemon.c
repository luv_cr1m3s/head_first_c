#include <stdio.h>

typedef union{
    float lemon;
    int lemon_pieces;
} lemon_lime;

typedef struct {
    float tequila;
    float cointrenau;
    lemon_lime cytrus;
} margarita;

int main(){

    margarita m = {2.0, 1.0, {2}};
    printf("%2.1f measures of tequila\n%2.1f measures of cointrenau\n%2.1f measures of juice\n",
            m.tequila, m.cointrenau, m.cytrus.lemon);

    return 0;
}

