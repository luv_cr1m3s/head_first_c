#include <stdio.h>

typedef struct{
    const char *name;
    const char *species;
    int age;
} turtle;

void happy(turtle t){
    t.age++;
    printf("Happy wrong Birhday %s! You arwe now: %i\n", t.name, t.age);
}

void happy_good(turtle *t){

    (*t).age += 1;
    printf("Happy right Birthday %s, you are now %i years old!\n", (*t).name, (*t).age);
}

int main(){

    turtle myrtle = {"Myrtle", "Leatherback", 99};
    happy(myrtle);
    printf("%s is now %i years old!\n",  myrtle.name, myrtle.age);
    happy_good(&myrtle);
    printf("%s after right Birthday you are %i years old!\n", myrtle.name, myrtle.age);
    return 0;
}
