#include <stdio.h>
#include <string.h>

char tracks[][80] = {
        "I left my heart with you",
        "Newark, Newark - a wonderful town",
        "Dancing with a Dork",
        "From ere to maternity",
        "The girl from Iwo Jima",
};                                      

void find_track(char search_for[]);

int main(){

    char search[80];
    printf("Search for: ");
    fgets(search, 80, stdin);
    
    char *p;
    if ((p = strrchr(search, '\n')) != NULL) {
        *p = 0;
    }

    find_track(search);
    return 0;
}

void find_track(char search[]){
    
    int i;
    for(i = 0; i < 5; i++){
        if(strstr(tracks[i], search))
            printf("Track %i: '%s'\n", i, tracks[i]);
    }
}
