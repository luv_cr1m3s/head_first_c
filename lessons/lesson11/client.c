#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <sys/types.h>

void error(char *msg){
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(1);
}

// 1. BIND A PORT
int open_socket(){
/*	struct addrinfo hints, *res;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = PF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	if(getaddrinfo(host, port, &hints, &res) == -1)
		error("Can't resolve the address.");
	
	int d_sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	*/
	int d_sock = socket(PF_INET, SOCK_STREAM, 0);
	struct sockaddr_in si;
	memset(&si, 0, sizeof(si));
	si.sin_family = PF_INET;
	si.sin_addr.s_addr = inet_addr("172.217.21.174");
	si.sin_port = htons(80);
	if (d_sock == -1)
		error("Can't open socket.");

	int c = connect(d_sock, (struct sockaddr *) &si, sizeof(si));

	if (c == -1)
		error("Can't connect to socket.");

	return d_sock;
}

int say(int socket, char *s){
	int result = send(socket, s, strlen(s), 0);
	if (result == -1)
		error("Error talking to the server.");

	return result;
}

int main(int argc, char *argv[]){
	// 2. CONNECT TO A REMOTE PORT
	int d_sock;
	d_sock = open_socket();
	char buf[255];

	sprintf(buf, "GET /search?q=%s HTTP/1.1\r\n", argv[1]);
	// wiki: sprintf(buf, "GET /wiki/%s HTTP/1.1\r\n", argv[1]);
	say(d_sock, buf);

	say(d_sock, "Host: www.google.com\r\n\r\n");
	char rec[256];
	int bytesRcvd = recv(d_sock, rec, 255, 0);
	while(bytesRcvd){
		if (bytesRcvd == -1)
			error("Can't read from server.");

		rec[bytesRcvd] = '\0';
		printf("%s", rec);
		bytesRcvd = recv(d_sock, rec, 255, 0);
	}

	close(d_sock);

	return 0;
}
