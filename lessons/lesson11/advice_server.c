#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <arpa/inet.h>

void error(char msg[]){
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(1);
}

int main(){

	char *advice[] = {
		"Take smaller bites\r\n",
		"Go for the tight jeans. No they do NOT make you look fat.\r\n",
		"One word: inappropriate\r\n",
		"Just for today, be honest. Tell your boss  what you *really* think\r\n",
		"You might want to rethink that haircut\r\n"
	};
	/* Create a new socket of type TYPE in domain DOMAIN, using
		 protocol PROTOCOL.  If PROTOCOL is zero, one is chosen automatically.
		 Returns a file descriptor for the new socket, or -1 for errors.  
		 extern int socket (int __domain, int __type, int __protocol) __THROW;*/
	int listener_d = socket(PF_INET, SOCK_STREAM, 0);
	
	if (listener_d == -1)
		error("Can't open socket!");

	struct sockaddr_in name;
	name.sin_family = PF_INET;
	name.sin_port = (in_port_t)htons(30000);
	name.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(listener_d, (struct sockaddr *) &name, sizeof(name)) == -1)
		error("Can't bind to socket.");
	/* Prepare to accept connections on socket FD.
     N connection requests will be queued before further requests are refused.
     Returns 0 on success, -1 for errors.  
     extern int listen (int __fd, int __n) __THROW;*/
	if (listen(listener_d, 10))
		error("Can't listen.");

	puts("Waiting for connection.\n");

	while(1){
		struct sockaddr_storage client_addr;
		unsigned int address_size = sizeof(client_addr);
		int connect_d = accept(listener_d, (struct sockaddr *) &client_addr, &address_size);
		if(connect_d == -1)
			error("Can't open socket for listening.");

		char *msg = advice[rand() % 5];
		if(send(connect_d, msg, strlen(msg), 0) == -1)
			error("Can't send a message.");
		/* Shut down all or part of the connection open on socket FD.
		HOW determines what to shut down:
		SHUT_RD   = No more receptions;
		SHUT_WR   = No more transmissions;
		SHUT_RDWR = No more receptions or transmissions.
		Returns 0 on success, -1 for errors.  
		extern int shutdown (int __fd, int __how) __THROW;*/
		if(shutdown(connect_d, SHUT_RDWR) == -1)
			error("Can't close connection.");
		}
	return 0;
}
