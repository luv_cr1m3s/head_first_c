#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <arpa/inet.h>

int listener_d;

void handle_shutdown(int sig){
	if (listener_d)
		shutdown(listener_d, SHUT_RDWR);

	fprintf(stderr, "Bye!\n");
	exit(0);
}

void error(char *msg){
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(1);
}

int open_listener_socket(){
	int s = socket(PF_INET, SOCK_STREAM, 0);
	if ( s == -1)
		error("Can't open socket!");

	return s;
}

int say(int socket, char *s){
	int result = send(socket, s, strlen(s), 0);
	if(result == -1)
		error("Error talking to the client!");

	return result;
}

void bind_to_port(int socket, int port){
	struct sockaddr_in name;
	
	name.sin_family = PF_INET;
	name.sin_port = (in_port_t)htons(port);
	
	int reuse = 1;
	if (setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(int)) == -1)
		error("Can't set the reuse option on the socket.");
	if (bind(socket, (struct sockaddr*) &name, sizeof(name)) == -1)
		error("Can't bind to socket.");
}

int main(){
	if(signal(SIGINT, handle_shutdown) == SIG_ERR)
		error("Can't set the interrupt handler.");

	listener_d = open_listener_socket();
	bind_to_port(listener_d, 30000);
	if(listen(listener_d, 10) == -1)
		error("Can't listen.");

	struct sockaddr_storage client_addr;
	unsigned int address_size = sizeof(client_addr);
	puts("Waiting for connection.");
	char buf[255];

	while(1){
		int connect_d = accept(listener_d, (struct sockaddr *) &client_addr, &address_size);
		if (connect_d == -1)
			error("Can't open socket for listening.");
	
		if(say(connect_d, "KNOK!KNOK!\r\n") != -1){
			read(connect_d, buf, sizeof(buf));
			if(strncmp("Who's there?", buf, 12))
				say(connect_d, "You should say 'Who's there?'");
			else{
				if(say(connect_d, "Oscar\r\n") != -1){
					read(connect_d, buf, sizeof(buf));
					if(strncmp("Oscar who?", buf, 10))
						say(connect_d, "You should say  'Oscar who?'\r\n");
					else
						say(connect_d, "Oscar silly question, you get a silly answer!\r\n");
				}
			}
		}
		shutdown(connect_d, SHUT_RDWR);
		}

	return 0;
}
	
